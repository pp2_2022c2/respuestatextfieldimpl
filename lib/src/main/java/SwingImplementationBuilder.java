import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextField;

import core_pp2.DiseñoFormulario;
import core_pp2.Item;
import interfaces.FormImplementation;

public class SwingImplementationBuilder implements FormImplementation {
 
	private DiseñoFormulario diseñoFormulario;
	
	@Override
	public void setDiseñoFormulario(DiseñoFormulario diseñoFormulario) {
		this.diseñoFormulario = diseñoFormulario;
	}
	
    public JFrame build(Map<JComponent, JComponent> mapComponents) {
    	SwingImplementation swingImplementation = new SwingImplementation();
    	swingImplementation.addToImplementation(mapComponents);
    	
    	return swingImplementation.getImplementatedFormulario();
    }
    
    public Map<JComponent, JComponent> createMapComponents(){
    	Map<JComponent, JComponent> mapComponets = new HashMap<JComponent, JComponent>();
    	
	    for (Item item : this.diseñoFormulario.listadoItems) {
	    	// Se crea campo TextField con la pregunta
	    	JTextField campoPregunta = new JTextField(item.getPregunta());
	    	
	    	// Se crea campo JComponent a partir de la respuesta del factory y el tipoRespuesta
			ItemFactory respuestaFactory = new ItemFactory();
			JComponent campoRespuesta = respuestaFactory.createItem(item.getTipo());
			
			mapComponets.put(campoPregunta, campoRespuesta);
    	}	
	    return mapComponets;
    }
}
