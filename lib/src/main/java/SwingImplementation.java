import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class SwingImplementation {

	private final JFrame implementationFormulario;
	
	public SwingImplementation() {
		this.implementationFormulario = new JFrame();
	}

	public JFrame getImplementatedFormulario() {
		return this.implementationFormulario;
	}

	public void addToImplementation(Map<JComponent, JComponent> mapComponent) {		
		for (Map.Entry<JComponent, JComponent>entry: mapComponent.entrySet() ){
			this.implementationFormulario.getContentPane().add(entry.getKey());
			this.implementationFormulario.getContentPane().add(entry.getValue());
		}
	}
}
