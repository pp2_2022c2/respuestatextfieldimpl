import javax.swing.*;

public class ItemFactory {
	public JComponent createItem(String tipoRespuesta) {
		if (tipoRespuesta == null || tipoRespuesta.isEmpty()) { 
			return null; 
		}
		
		switch (tipoRespuesta) {
			case "TextField": {
				return new JTextField();
			}
			case "ComboBox": {
				return new JComboBox<>();
			}
			case "CheckBox": {
				return new JCheckBox();
			}
			case "ScrollBar": {
				return new JScrollBar();
			}
			default: 
				throw new IllegalArgumentException("Tipo desconocido " + tipoRespuesta);
		}
	}
}
